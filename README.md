# EECS 581 Project

### Project Members

- Matthew Mehrmann
- Eric Kuebler
- Kevin Likcani
- Yucheng Chen
- Jacky Lin

## Getting Started

### 1. Install Necessary Applications

Install a text editor of choice, such as [Visual Studio Code](https://code.visualstudio.com/).

Install [Unity](https://unity.com/download).

### 2. Clone Project Repository

Using git, clone [this project](https://gitlab.com/kuebler324/eecs581).

### 3. Open Project In Unity

In Unity Hub, select open in the top right. Select the root folder (named `eecs581`) and allow Unity to install the necessary modules.

## Development

We will be using [Gitlab](https://gitlab.com/) for version control and [Jira](https://www.atlassian.com/software/jira) for project management.

### Link to our project's [Gitlab](https://gitlab.com/kuebler324/eecs581)

### Link to our project's [Jira](https://eecs581.atlassian.net/jira/software/projects/ID/boards/1)
