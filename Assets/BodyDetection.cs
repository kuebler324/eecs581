using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BodyDetection : MonoBehaviour
{
    //localScale = transform.localScale; //captures local scale
    //rb = GetComponent<Rigidbody2D>(); //captures rigidbody
    GameObject snake; //defines gameObject snake

    // Start is called before the first frame update
    void Start()
    {
        //assigns snake to the snake object
    }

    private void OnCollisionEnter2D(Collision2D collision)//called when collision occurs with this object
    {
        if(collision.gameObject.CompareTag("Player"))
        {
            SceneManager.LoadScene("Game Over Menu");
        }
    }
}
