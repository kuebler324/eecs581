/*
MovingPlatform.cs
This is the script to make the platform move and along with player
Authors: Jacky Lin
Created: 11/5/2023
Revised:
Jacky Lin: 11/5/2023 - add moving functionality to cloud
Jacky Lin: 11/10/2023 - make cloud passable from under
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : MonoBehaviour
{
    // platform speed
    public float speed;
    // start position
    public int start;
    // positions where platform moves
    public Transform[] positions;
    // index of array
    private int i;
    // Start is called before the first frame update
    void Start()
    {
        // sets starting point
        transform.position = positions[start].position;    
    }

    // Update is called once per frame
    void Update()
    {
        if (Vector2.Distance(transform.position, positions[i].position) < 0.05f)
        {
            i++;
            // reset if reaches last point
            if (i == positions.Length)
            {
                i = 0;
            }
        }

        // move platform towards position of current index
        transform.position = Vector2.MoveTowards(transform.position, positions[i].position, speed * Time.deltaTime);
    }

    // move player with cloud
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player"))
        {
            if (transform.position.y < collision.transform.position.y-1f)
                collision.transform.SetParent(transform);
        }
            
    }

    // stop moving with cloud
    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Player"))
            collision.transform.SetParent(null);
    }
}
