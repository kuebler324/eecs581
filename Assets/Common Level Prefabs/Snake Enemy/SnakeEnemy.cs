using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeEnemy : MonoBehaviour
{
    public float movementSpeed = 3f; //defines and sets movement speed
    public Transform leftWayPoint; //defines variable for left marker
    public Transform rightWayPoint; //defines variable for right marker
    public HeadDetection headDetection; // variable for head detection
    Vector3 localScale; //defines variable for local scale
    bool movingRight = false; //defines boolean that checks if snake is moving right. set to false
    Rigidbody2D rb; //defines rigidbody variable

    // Start is called before the first frame update
    void Start()
    {
        localScale = transform.localScale; //captures local scale
        rb = GetComponent<Rigidbody2D>(); //captures rigidbody
    }

    // Update is called once per frame
    void Update()
    {
        if(headDetection.SnakeCollision == true)//sets SnakeCollision boolean to true
        {
            stopMoving();//calls stopMoving method
        }

        if(transform.position.x > rightWayPoint.position.x)//checks if x position of snake is greater than right marker value
        {
            movingRight = false;//sets movingRight boolean to false
        }
        if(transform.position.x < leftWayPoint.position.x)//checks if x position of snake is less than left marker value
        {
            movingRight = true;//sets movingRight boolean to true
        }

        if(movingRight)//checks if movingRight boolean is equal to true
        {
            moveRight(); //calls moveRight method
        }
        else
        {
            moveLeft(); //calls moveLeft method
        }  
    }

    void moveRight() //moveRight method that handles moving the snake to the right
    {
        movingRight = true; //sets movingRight boolean to true
        localScale.x = 1; //sets localScale of x-axis equal to 1
        transform.localScale = localScale; //sets transform.localScale equal to localScale to move snake along
        rb.velocity = new Vector2(localScale.x * movementSpeed, rb.velocity.y); //sets rigidbody velocity to new value based on scale and movement speed
    }
    void moveLeft() //moveLeft method that handles moving the snake to the left
    {
        movingRight = false; //sets movingRight boolean to true
        localScale.x = -1; //sets localScale of x-axis equal to 1
        transform.localScale = localScale; //sets transform.localScale equal to localScale to move snake along
        rb.velocity = new Vector2(localScale.x * movementSpeed, rb.velocity.y); //sets rigidbody velocity to new value based on scale and movement speed
    }

    void stopMoving() //stopMoving method that handles stopping the snake's movement
    {
        rb.constraints = RigidbodyConstraints2D.FreezePositionX; //freezes rigidbody of snake to stop movement
    }

}
