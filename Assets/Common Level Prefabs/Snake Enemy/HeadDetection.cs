using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class HeadDetection : MonoBehaviour
{
    GameObject snake; //defines gameObject snake
    public bool SnakeCollision = false; //defines variable that detects snake collision

    // Start is called before the first frame update
    void Start() 
    {
        snake = gameObject.transform.parent.gameObject; //assigns snake to the snake object
    }

    private void OnCollisionEnter2D(Collision2D collision)//called when collision occurs with this object
    {
        snake.GetComponent<SpriteRenderer>().flipY = true; //flips snake upside-down
        SnakeCollision = true; //sets SnakeCollision bool equal to true
        StartCoroutine(death()); //Starts Coroutine that calls death method
    }

    private IEnumerator death() //handles snake death
    {
        yield return new WaitForSeconds(0.25f); //delays 1 second
        Destroy(snake.gameObject); //destroys snake
    }
}
