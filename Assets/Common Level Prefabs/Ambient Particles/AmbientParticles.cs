using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AmbientParticles : MonoBehaviour
{
    // particle system
    public ParticleSystem ps;
    // camera
    public Camera camera;

    // Update is called once per frame
    void Update()
    {
        // adjust the particle system position to remain on screen
        var cameraPosition = camera.transform.position;
        var shape = ps.shape;
        shape.position = new Vector3(cameraPosition.x * 2, cameraPosition.y * 2 + 20, shape.position.z);
    }
}
