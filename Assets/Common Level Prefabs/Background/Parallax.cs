using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax : MonoBehaviour
{
    public float xParallaxAmt; //defining the x-axis parallaxAmt public float variable
    public float yParallaxAmt; //defining the y-axis parallaxAmt public float variable

    private float xLength; //defining the length private float variable of x-axis length
    private float yLength; //defining the length private float variable for y-axis length
    private float xStartingPosition; //defining the x-axis startingPosition private float variable
    private float yStartingPosition; //defining the y-axis startingPosition private float variable

    public GameObject cam; //defining the cam GameObject
    void Start() // Start is called before the first frame update
    {
        xStartingPosition = transform.position.x; //sets the starting position to the position of the object at start
        xLength = GetComponent<SpriteRenderer>().bounds.size.x; //sets the length to the size of the object at start
    
        yStartingPosition = transform.position.y - 1; //sets the starting position to the position of the object at start
        yLength = GetComponent<SpriteRenderer>().bounds.size.y; //sets the length to the size of the object at start
    
    }

    void FixedUpdate() //runs at constant rate
    {
        float distance = (cam.transform.position.x * xParallaxAmt); //sets distance equal to the camera's position times the parallax amount
        float yDistance = (cam.transform.position.y * yParallaxAmt); //sets distance equal to the camera's position times the parallax amount

        transform.position = new Vector3(xStartingPosition + distance, yStartingPosition + yDistance, transform.position.z); //updates the x position to be the starting position + distance
    }
}
