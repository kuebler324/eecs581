using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingPlatform : MonoBehaviour
{
    private float fallingDelay = 1f; //defines variable for falling delay and sets it to 1f
    private float destroyingDelay = 2f; //defines variable for destroying delay and sets it to 2f 

    [SerializeField] private Rigidbody2D rigid; //defines variable of a RigidBody2D type for rigid

    private void OnCollisionEnter2D(Collision2D collision) //defines OnCollisionEnter2D method with collision parameter
    {
        if (collision.gameObject.CompareTag("Player")) //checks if object collides with object with the "Player" tag
        {
            StartCoroutine(Falling()); //initiates the falling of the object
        }
    }

    private IEnumerator Falling() //defines Falling method
    {
        yield return new WaitForSeconds(fallingDelay); //Waits for amount of seconds that the fallingDelay value holds
        rigid.bodyType = RigidbodyType2D.Dynamic; //Sets the rigid body type to dynamic
        Destroy(gameObject, destroyingDelay); //Destroys the game object
    }
}
