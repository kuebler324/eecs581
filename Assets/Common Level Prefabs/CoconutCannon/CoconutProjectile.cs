using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CoconutProjectile : MonoBehaviour
{
    private float despawnTime = 0.0f;
    // Start is called before the first frame update
    void Start()
    {
        despawnTime = Time.time + 60.0f;
    }

    // Update is called once per frame
    void Update()
    {
        float direction = transform.localScale.x < 0.0f ? -1.0f : 1.0f; // direction
        float mult = Time.deltaTime * direction; // time and direction
        // spin the coconut
        transform.Rotate(new Vector3(0.0f, 0.0f, -360.0f) * mult);
        // move the coconut forward
        transform.position += new Vector3(8.0f, 0.0f, 0.0f) * mult;
        if(despawnTime <= Time.time) {
            // delete projectile if it has been a long time
            Object.Destroy(this.gameObject);
        }
    }
    private void OnCollisionEnter2D(Collision2D collision) //defines OnCollisionEnter2D method with collision parameter
    {
        if (collision.gameObject.CompareTag("Player")) //checks if object collides with object with the "Player" tag
        {
            // trigger game over screen
            SceneManager.LoadScene("Game Over Menu");
        }
        // destroy coconut self on collision
        Object.Destroy(this.gameObject);
    }
}
