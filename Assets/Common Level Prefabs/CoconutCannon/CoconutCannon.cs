using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoconutCannon : MonoBehaviour
{
    public GameObject coconutPrefab; // store the coconut so that the cannon knows what to spawn in 
    // Start is called before the first frame update
    private float nextFire;
    private float fireRate = 4.0f;
    void Start()
    {
        // wait to fire  coconut
        nextFire = Time.time + fireRate;
    }

    // Update is called once per frame
    void Update()
    {
        // if wait is over, fire a coconut
        if(nextFire <= Time.time) {
            nextFire = Time.time + fireRate; // set the timer for the next coconut
            float direction = transform.localScale.x; // get direction from scale
            GameObject coconut = Instantiate(coconutPrefab, transform.position + new Vector3(0.8f * direction, 0.65f, 0.0f), Quaternion.identity); // fire another coconut
            Vector3 localScale = coconut.transform.localScale;
            coconut.transform.localScale = new Vector3(localScale.x * direction, localScale.y, localScale.z);
        }
    }
}
