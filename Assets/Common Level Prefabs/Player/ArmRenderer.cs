/*
Rope.cs
This is the line renderer for the arms mechanic
Authors: Eric Kuebler
Created: 10/18/2023
Revised:
*/
using UnityEngine;
public class ArmRenderer : MonoBehaviour
{
    // here are some of the params regarding the line rendering, including references required for the arms to render correctly
    [Header("General Refernces:")]
    public Arms arms;
    public LineRenderer lineRendererLeft;
    public LineRenderer lineRendererRight;

    [Header("General Settings:")]
    [SerializeField] private int precision = 40;
    [Range(0, 20)][SerializeField] private float straightenLineSpeed = 5;

    [Header("Rope Animation Settings:")]
    public AnimationCurve ropeAnimationCurve;
    [Range(0.01f, 4)][SerializeField] private float StartWaveSize = 2;
    float waveSize = 0;

    [Header("Rope Progression:")]
    public AnimationCurve ropeProgressionCurve;
    [SerializeField][Range(1, 50)] private float ropeProgressionSpeed = 1;

    float moveTime = 0;

    [HideInInspector] public bool isGrappling = true;

    bool straightLine = true;
    private Vector3 leftArmOffset = new Vector3(0.35f, -0.2f, 0);
    private Vector3 rightArmOffset = new Vector3(-0.35f, -0.2f, 0);
    private Vector2 leftGrappleOffset = new Vector2(0.2f, 0);
    private Vector2 rightGrappleOffset = new Vector2(-0.2f, 0);

    private void OnEnable()
    {
        // enable the rope, init the following values to be used
        moveTime = 0;
        lineRendererLeft.positionCount = precision;
        lineRendererRight.positionCount = precision;
        waveSize = StartWaveSize;
        straightLine = false;

        // set the initial positions of each point on the line renderer
        for (int i = 0; i < precision; i++)
        {
            lineRendererLeft.SetPosition(i, arms.firePoint.position + leftArmOffset);
            lineRendererRight.SetPosition(i, arms.firePoint.position + rightArmOffset);
        }
        // enable line rendering
        lineRendererLeft.enabled = true;
        lineRendererRight.enabled = true;
    }

    private void OnDisable()
    {
        // disable the rendering of the rope
        lineRendererLeft.enabled = false;
        lineRendererRight.enabled = false;
        isGrappling = false;
    }

    private void Update()
    {
        moveTime += Time.deltaTime * 1.5f; // increment move time for this usage of the grapple
        if (!straightLine)
        {   // animate the grappling point moving toward its grapple point
            if (Mathf.Abs(lineRendererLeft.GetPosition(precision - 1).x - arms.grapplePoint.x - leftGrappleOffset.x) < 0.01)
            {
                straightLine = true; // collide with grapple point
            }
            else
            {
                DrawArmExtend(); // draw the rope (moving)
            }
        }
        else
        {
            if (!isGrappling) // if not grappling, set grappling to true. Do grapple in arms
            {
                arms.Grapple();
                isGrappling = true;
            }
            waveSize -= Time.deltaTime * straightenLineSpeed; // decrement the waviness of the line
            if (waveSize > 0)
            {
                DrawArmExtend(); // draw the arms (moving)
            }
            else
            {
                waveSize = 0;
                // conver the draw into a line
                if (lineRendererLeft.positionCount != 2)
                {

                    lineRendererLeft.positionCount = 2;
                }
                if (lineRendererRight.positionCount != 2)
                {

                    lineRendererRight.positionCount = 2;
                }

                DrawArmStatic(); // draw the arms (just a line)
            }
        }
    }

    void DrawArmExtend()
    {
        // set the position of each point on the line renderer
        for (int i = 0; i < precision; i++)
        {
            float delta = (float)i / ((float)precision - 1f); // determine a value to be used in lerp functions for the point between the start and finish
            Vector2 offset = Vector2.Perpendicular(arms.grappleDistanceVector).normalized * ropeAnimationCurve.Evaluate(delta) * Mathf.Sin(moveTime) * waveSize; // use the animation curve to offset the arms so long as it has not collided

            // left arm
            Vector2 targetPositionLeft = Vector2.Lerp(arms.firePoint.position + leftArmOffset, arms.grapplePoint + leftGrappleOffset, delta) + offset; // calculate target position for point
            Vector2 currentPositionLeft = Vector2.Lerp(arms.firePoint.position + leftArmOffset, targetPositionLeft, ropeProgressionCurve.Evaluate(moveTime) * ropeProgressionSpeed); // calculate position
            lineRendererLeft.SetPosition(i, currentPositionLeft); // set the position

            // right arm
            Vector2 targetPositionRight = Vector2.Lerp(arms.firePoint.position + rightArmOffset, arms.grapplePoint + rightGrappleOffset, delta) + offset; // calculate target position for point
            Vector2 currentPositionRight = Vector2.Lerp(arms.firePoint.position + rightArmOffset, targetPositionRight, ropeProgressionCurve.Evaluate(moveTime) * ropeProgressionSpeed); // calculate position
            lineRendererRight.SetPosition(i, currentPositionRight); // set the position
        }
    }

    void DrawArmStatic()
    {
        // draw a simple line between the start and end
        lineRendererLeft.SetPosition(0, arms.firePoint.position + leftArmOffset);
        lineRendererLeft.SetPosition(1, arms.grapplePoint + leftGrappleOffset);

        lineRendererRight.SetPosition(0, arms.firePoint.position + rightArmOffset);
        lineRendererRight.SetPosition(1, arms.grapplePoint + rightGrappleOffset);
    }
}