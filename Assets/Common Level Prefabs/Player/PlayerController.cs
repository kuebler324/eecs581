/*
PlayerController.cs
This is the event controller for key presses that constitute to player movement
Authors: Yucheng Chen, Jacky Lin, Eric Kuebler
Created: 9/21/2023
Revised:
Yucheng Chen: 9/21/2023 - left and right movement
Jacky Lin: 9/21/2023 - jumping
Eric Kuebler: 10/4/2023 - improve feel of movement
Jacky Lin: 10/21/2023 - add banana scores
Eric Kuebler: 10/30/2023 - add jumping to arm swinging
Jacky Lin: 12/3/2023 - add power up speed up/slow down
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using TMPro;

[RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]

public class PlayerController : MonoBehaviour
{
    // rigid body and collision components
    private Rigidbody2D _rb;
    private CapsuleCollider2D _col;
    // sprite renderer component
    private SpriteRenderer[] _sprites;
    // velocity of player, per frame
    private Vector2 _frameVelocity;
    // switch directions when in mirror region
    private int _mirrorMultiplier;
    // the current frame, allowing us to determine how many frames have passed since an event
    private int _fixedFrame;
    // player animator
    private Animator _animator;

    public ParticleSystem DustP;
    // scoring system
    public TextMeshProUGUI scoreText;
    public GameObject bananas;
    private int _score;
    private int _totalBananas;
    public ArmRenderer armRenderer;
    public Arms arms;

    // used for resetting timer on power ups
    private Coroutine _speedRoutine;
    //sound effect
    [SerializeField] private AudioSource jumpSoundEffect;
    [SerializeField] private AudioSource collectionSoundEffect;
    [SerializeField] public AudioSource drumSoundEffect;
    [SerializeField] private AudioSource speedUpSoundEffect;
    [SerializeField] private AudioSource slowDownSoundEffect;
    [SerializeField] private AudioSource mudSoundEffect;
    [SerializeField] private AudioSource mirrorSoundEffect;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody2D>(); // get the player rigid body
        _col = GetComponent<CapsuleCollider2D>(); // get the player collider
        _sprites = GetComponentsInChildren<SpriteRenderer>(); // get sprite renderer
        _animator = GetComponent<Animator>();   // get animator
        // get total bananas
        _totalBananas = bananas.transform.childCount;
        _score = 0;
        scoreText.text = _score.ToString() + " / " + _totalBananas.ToString();
        _originalSpeed = statMaxSpeed;
        _mirrorMultiplier = 1;
    }

    private bool _inputJump = false; // bool saying whether jump is active
    private float _inputWalk = 0; // float determining x change for walk based on controls

    public void OnMove(InputAction.CallbackContext context)
    {
        _inputWalk = context.ReadValue<Vector2>().x * _mirrorMultiplier; // set the walk input based on direction of movement
    }

    public void OnJump(InputAction.CallbackContext context)
    {
        _inputJump = context.ReadValue<float>() == 1; // get the jump input
        if (_inputJump)
        {
            // if jump input, set the necessary jump info
            _jumpToConsume = true;
            _frameJumpWasPressed = _fixedFrame;
        }
    }

    private void HandleGameOver()
    {
        float fallOffThreshold = -20;
        // checks if player falls below threshold
        if (transform.position.y < fallOffThreshold)
        {
            // goes to game over scene
            SceneManager.LoadScene("Game Over Menu");
        }
    }

    private void FixedUpdate()
    {
        // checks if player falls off
        HandleGameOver();

        // increment the current frame
        _fixedFrame++;

        // run the necessary movement functions, collision, jump, horizontal movement, vertical movement, and then finally apply the calculated movements
        CheckCollisions();

        HandleJump();
        HandleHorizontal();
        HandleVertical();

        // apply the calculated velocity
        _rb.velocity = _frameVelocity;

        // make the player face the direction they are moving
        if (_frameVelocity.x != 0)
        {
            // determine x scale based on velocity
            Vector2 scale = new Vector2(Mathf.Abs(transform.localScale.x) * (_frameVelocity.x < 0 ? -1 : 1), transform.localScale.y);
            transform.localScale = scale;
        }

        // play dust particles on ground
        if (_grounded) DustP.Play();
        else DustP.Stop();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        // checks collision into banana
        if (other.gameObject.CompareTag("Banana"))
        {
            collectionSoundEffect.Play();
            //update score text
            scoreText.text = (++_score).ToString() + " / " + _totalBananas.ToString();
            // destroy banana
            Destroy(other.gameObject);

            // win condition
            if (_score == _totalBananas)
            {
                SceneManager.LoadScene("Win Menu");
            }
        }
        else if (other.gameObject.CompareTag("Drums"))
        {
            drumSoundEffect.Play();
        }
        else if (other.gameObject.CompareTag("Mud"))
        {
            mudSoundEffect.Play();
        }
        else if (other.gameObject.CompareTag("SpeedUp"))
        {
            foreach (SpriteRenderer s in _sprites)
                s.color = new Color(1f, 0.7372549f, 0.7137255f);
            speedUpSoundEffect.Play();
            // double speed
            statMaxSpeed = _originalSpeed * 2;
            // stop if exists
            if (this._speedRoutine != null)
                StopCoroutine(this._speedRoutine);

            // start new
            this._speedRoutine = StartCoroutine(RevertSpeed(5));
        }
        else if (other.gameObject.CompareTag("SlowDown"))
        {
            foreach (SpriteRenderer s in _sprites)
                s.color = new Color(0.4198113f, 0.7634338f, 1f);
            slowDownSoundEffect.Play();
            // half speed
            statMaxSpeed = _originalSpeed / 2;
            // stop if exists
            if (this._speedRoutine != null)
                StopCoroutine(this._speedRoutine);

            // start new 
            this._speedRoutine = StartCoroutine(RevertSpeed(5));
        }
        //check for collision on rocks or wooden spikes
        else if (other.gameObject.CompareTag("WoodenSpikes"))
        {
            //end level if contact is made.
            SceneManager.LoadScene("Game Over Menu");
        }

        else if (other.gameObject.CompareTag("stone"))
        {
            //end level if contact is made.
            SceneManager.LoadScene("Game Over Menu");
        }
    
        // flip controls on entering
        else if (other.gameObject.CompareTag("MirrorRegion"))
        {
            _mirrorMultiplier = -1;
            mirrorSoundEffect.Play();
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        // unflip controls
        if (other.gameObject.CompareTag("MirrorRegion"))
        {
            _mirrorMultiplier = 1;
        }
    }

    IEnumerator RevertSpeed(float time)
    {
        var instruction = new WaitForEndOfFrame();

        // count down
        while (time > 0)
        {
            time -= Time.deltaTime;
            yield return instruction;
        }

        // revert speed
        statMaxSpeed = _originalSpeed;
        foreach (SpriteRenderer s in _sprites)
            s.color = new Color(1f, 1f, 1f);
    }

    #region Collisions
    private int _frameLeftGrounded = int.MinValue;
    private bool _grounded;

    // variables related to box casts for 
    private Vector2 _boxSize = new Vector2(0.5f, 0.2f);
    private float _upCastRange = 1.1f;
    private float _downCastRange = 1.3f;

    private void OnDrawGizmos()
    {
        if (_col)
        {
            // draw the collision zones for ceiling and floor
            Gizmos.DrawWireCube(transform.position + Vector3.up * _upCastRange, _boxSize);
            Gizmos.DrawWireCube(transform.position + Vector3.down * _downCastRange, _boxSize);
        }
    }

    private void CheckCollisions()
    {

        // Ground and Ceiling (down and up) box casts in order to determine collision zones
        bool upCast = false;
        bool downCast = false;
        bool drumCast = Physics2D.BoxCast(transform.position, _boxSize, 0, Vector2.down, _downCastRange, statDrumLayer);
        if (Physics2D.BoxCast(transform.position, _boxSize, 0, Vector2.up, _upCastRange, statGroundLayer))
        {
            upCast = true;
        }
        if (Physics2D.BoxCast(transform.position, _boxSize, 0, Vector2.down, _downCastRange, statGroundLayer | statCloudLayer) || drumCast)
        {
            downCast = true;
        }

        // check for drum collision
        if (drumCast)
        {
            // play drum sound
            drumSoundEffect.Play();

            // adjust y velocity 
            _frameVelocity.y = statJumpPower * 2f;
        }

        // Hit a Ceiling
        if (upCast)
        {
            // prevent player from moving up any more
            _frameVelocity.y = Mathf.Min(0, _frameVelocity.y);
        }

        // Landed on the Ground
        if (!_grounded && downCast)
        {
            // set variables related to being on the ground
            _grounded = true;
            _coyoteUsable = true;
            _bufferedJumpUsable = true;
            _endedJumpEarly = false;
            //_animator.SetBool("isJumping", false);
        }
        // Left the Ground
        else if (_grounded && !downCast)
        {
            // set variables that convey player is no longer on ground
            _grounded = false;
            _frameLeftGrounded = _fixedFrame;
            // _animator.SetBool("isJumping", true);
        }

    }

    #endregion


    #region Jumping

    private bool _jumpToConsume = false;
    private bool _bufferedJumpUsable;
    private bool _endedJumpEarly;
    private bool _coyoteUsable;
    private int _frameJumpWasPressed;

    private bool HasBufferedJump => _bufferedJumpUsable && _fixedFrame < _frameJumpWasPressed + statJumpBufferFrames;
    private bool CanUseCoyote => _coyoteUsable && !_grounded && _fixedFrame < _frameLeftGrounded + statCoyoteFrames;

    private void HandleJump()
    {

        // determine if jump should end early
        if (!_endedJumpEarly && !_grounded && !_inputJump && _rb.velocity.y > 0) _endedJumpEarly = true;

        // prevent jump
        if (!_jumpToConsume && !HasBufferedJump) return;

        // do the jump if possible
        if (_grounded || CanUseCoyote || armRenderer.isGrappling)
            ExecuteJump();

        _jumpToConsume = false;
    }

    private void ExecuteJump()
    {
        jumpSoundEffect.Play();
        // modify the following vars to prevent further jumping
        _endedJumpEarly = false;
        _frameJumpWasPressed = 0;
        _bufferedJumpUsable = false;
        _coyoteUsable = false;
        // set y velocity for jump
        _frameVelocity.y = statJumpPower;
        if (armRenderer.isGrappling) // disable grapple if currently grappling
        {
            arms.UnGrapple();
        }
    }

    #endregion

    #region Horizontal

    private void HandleHorizontal()
    {
        if (_inputWalk == 0)
        {
            // decrease acceleration
            var deceleration = _grounded ? statGroundDeceleration : statAirDeceleration;
            _frameVelocity.x = Mathf.MoveTowards(_frameVelocity.x, 0, deceleration * Time.fixedDeltaTime);
            _animator.SetBool("isRunning", false);
        }
        else
        {
            // walk, increase acceleration
            _frameVelocity.x = Mathf.MoveTowards(_frameVelocity.x, _inputWalk * statMaxSpeed, statAcceleration * Time.fixedDeltaTime);
            _animator.SetBool("isRunning", true);
        }
    }

    #endregion

    #region Vertical

    private void HandleVertical()
    {
        // handle vertical movement
        if ((_grounded || armRenderer.isGrappling) && _frameVelocity.y <= 0f)
        {
            // use grounding force to stay slightly off the ground
            _frameVelocity.y = statGroundingForce;
        }
        else
        {
            // apply gravity
            var inAirGravity = statFallAcceleration;
            if (_endedJumpEarly && _frameVelocity.y > 0) inAirGravity *= statJumpEndEarlyGravityModifier;
            _frameVelocity.y = Mathf.MoveTowards(_frameVelocity.y, -statMaxFallSpeed, inAirGravity * Time.fixedDeltaTime);
        }
    }

    #endregion

    [Header("LAYERS")]
    [Tooltip("Set this to the layer of the ground")]
    public LayerMask statGroundLayer;
    public LayerMask statCloudLayer;
    public LayerMask statDrumLayer;

    [Header("MOVEMENT")]
    [Tooltip("The top horizontal movement speed")]
    public float statMaxSpeed = 14;
    private float _originalSpeed;

    [Tooltip("The player's capacity to gain horizontal speed")]
    public float statAcceleration = 120;

    [Tooltip("The pace at which the player comes to a stop")]
    public float statGroundDeceleration = 60;

    [Tooltip("Deceleration in air only after stopping input mid-air")]
    public float statAirDeceleration = 30;

    [Tooltip("A constant downward force applied while grounded. Helps on slopes"), Range(0f, -10f)]
    public float statGroundingForce = -1.5f;

    [Header("JUMP")]
    [Tooltip("The immediate velocity applied when jumping")]
    public float statJumpPower = 36;

    [Tooltip("The maximum vertical movement speed")]
    public float statMaxFallSpeed = 40;

    [Tooltip("The player's capacity to gain fall speed. a.k.a. In Air Gravity")]
    public float statFallAcceleration = 110;

    [Tooltip("The gravity multiplier added when jump is released early")]
    public float statJumpEndEarlyGravityModifier = 3;

    [Tooltip("The fixed frames before coyote jump becomes unusable. Coyote jump allows jump to execute even after leaving a ledge")]
    public int statCoyoteFrames = 7;

    [Tooltip("The amount of fixed frames we buffer a jump. This allows jump input before actually hitting the ground")]
    public int statJumpBufferFrames = 7;
}
