/*
Arms.cs
This is the script for the logic of the arms mechanic
Authors: Eric Kuebler
Created: 10/18/2023
Revised:
*/
using UnityEngine;

public class Arms : MonoBehaviour
{
    // the following are some variables associated with how the arms mechanic works and references needed for the mechanic to work appropriately
    [Header("Scripts Ref:")]
    public ArmRenderer armRenderer;

    [Header("Layers Settings:")]
    [SerializeField] private int grappableLayerNumber = 9;

    [Header("Main Camera:")]
    public Camera camera;

    [Header("Transform Ref:")]
    public Transform player;
    public Transform pivot;
    public Transform firePoint;

    [Header("Physics Ref:")]
    public SpringJoint2D springJoint2D;
    public Rigidbody2D rigidbody;

    [Header("Distance:")]
    [SerializeField] private float maxDistance = 20;

    [Header("Launching:")]
    [SerializeField] private float launchSpeed = 1;
    [HideInInspector] public Vector2 grapplePoint;
    [HideInInspector] public Vector2 grappleDistanceVector;

    private void Start()
    {
        // disable the rendering of the arms
        armRenderer.enabled = false;
        // disable spring joint
        springJoint2D.enabled = false;
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            // if mouse down at start, set grapple point
            SetGrapplePoint();
        }
        else if (Input.GetKey(KeyCode.Mouse0))
        {
            // if mouse is down, continue grappling
            if (armRenderer.enabled)
            {
                Rotate(grapplePoint); // rotate direction of grapple based on grapple point
            }
            else
            {
                Vector2 mousePos = camera.ScreenToWorldPoint(Input.mousePosition);
                Rotate(mousePos); // rotate direction of grapple based on mouse position
            }
        }
        else if (Input.GetKeyUp(KeyCode.Mouse0))
        {
            UnGrapple();
        }
        else
        {
            Vector2 mousePos = camera.ScreenToWorldPoint(Input.mousePosition);
            Rotate(mousePos); // rotate direction of grapple based on mouse position
        }
    }

    void Rotate(Vector3 lookPoint)
    {
        // calculate distance based on mouse and pivot
        Vector3 distanceVector = lookPoint - pivot.position;
        // calculate angle and set pivot rotation
        float angle = Mathf.Atan2(distanceVector.y, distanceVector.x) * Mathf.Rad2Deg;
        pivot.rotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    void SetGrapplePoint()
    {
        // do the grappling
        Vector2 distanceVector = camera.ScreenToWorldPoint(Input.mousePosition) - pivot.position;
        int layerMask = 1 << grappableLayerNumber;
        // create layer mask to use for raycast based on only colliding with the grappable layer
        RaycastHit2D hit = Physics2D.Raycast(firePoint.position, distanceVector.normalized, maxDistance, layerMask); // send out raycast from firepoint in direction of the distanceVector
        if (hit)
        {
            // if collision, set the grapple point and calculate the distance vector, enable rope
            grapplePoint = hit.point;
            grappleDistanceVector = grapplePoint - (Vector2)pivot.position;
            armRenderer.enabled = true;
        }
    }
    public void UnGrapple()
    {

        // disable grappling, by disabling rendering and disabling the spring joint
        armRenderer.enabled = false;
        springJoint2D.enabled = false;
        rigidbody.gravityScale = 1;
    }

    public void Grapple()
    {
        // do grappling
        springJoint2D.autoConfigureDistance = false;

        // TODO: improve the integration of this with player movement / gravity
        // do launching
        springJoint2D.connectedAnchor = grapplePoint;

        Vector2 distanceVector = firePoint.position - player.position;

        springJoint2D.distance = distanceVector.magnitude;
        springJoint2D.frequency = launchSpeed;
        springJoint2D.enabled = true;
    }

    private void OnDrawGizmosSelected()
    {
        // use this to draw gizmos for the range of the player grapple
        if (firePoint != null)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawWireSphere(firePoint.position, maxDistance);
        }
    }

}