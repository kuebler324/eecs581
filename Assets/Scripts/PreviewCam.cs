/*
PreviewCam.cs
This is the event controller for switching between preview and main camera
Authors: Jacky Lin
Created: 4/12/2023
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PreviewCam : MonoBehaviour
{
    public GameObject mainCam;
    public GameObject previewCam;
    public int previewTime;
    // Start is called before the first frame update
    void Start()
    {
        // start the countdown for preview camera
        StartCoroutine(Preview());
    }

    // changes to main camera after 5 seconds
    IEnumerator Preview() {
        yield return new WaitForSeconds(previewTime);
        mainCam.SetActive(true);
        previewCam.SetActive(false);
    }
}
