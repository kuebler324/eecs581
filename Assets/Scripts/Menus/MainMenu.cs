/*
GameOverMenu.cs
Script for game over
Authors: Matthew Mehrmann, Jacky Lin
Created: 10/8/2023
Revised:
Matthew Mehrmann: 10/8/2023 - create button click functions
Jacky Lin: 10/8/2023 - move to main menu scene
            11/19/2023 - add sound toggle
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{
    public GameObject mainMenuUI; //mainMenu UI game object
    public GameObject settingsMenuUI; //generic settings menu
    public Toggle musicToggle;

    void Start()
    {
        //Add listener for when the state of the Toggle changes
        musicToggle.onValueChanged.AddListener(delegate {
            musicToggleChanged(musicToggle);
        });

        // add to player pref
        if (!PlayerPrefs.HasKey("musicOn"))
        {
            PlayerPrefs.SetInt("musicOn", 1);
        }
        Load();
        AudioListener.pause = !musicToggle.isOn;
    }

    private void CloseMenus()
    {
        mainMenuUI.SetActive(false);
        settingsMenuUI.SetActive(false);
    }

    public void LoadGeneralSettings() //Settings method called when settings button is clicked
    {
        CloseMenus();
        settingsMenuUI.SetActive(true);
    }

    //shows main menu
    public void LoadMenu() 
    { 
        CloseMenus();
        SceneManager.LoadScene("Main Menu");
    }

    public void LoadMain()
    {
        SceneManager.LoadScene("Level Menu");
    }

    private void Save()
    {
        PlayerPrefs.SetInt("musicOn", musicToggle.isOn ? 1 : 0);
    }

    private void Load()
    {
        musicToggle.isOn = PlayerPrefs.GetInt("musicOn") == 1;
    }

    void musicToggleChanged(Toggle change)
    {
        AudioListener.pause = !change.isOn;
        Save();
    }
}
