/*
LevelMenu.cs
Script for level menu
Authors: Jacky Lin
Created: 1/11/2024
*/
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelMenu : MonoBehaviour 
{
    public GameObject LevelButton;
    public Transform Spacer;
    public int levels;

    void Start ()
    {
        GenerateButtons();
	}

    // Generate all buttons
	private void GenerateButtons()
    {
        for (int i = 0; i < levels; i++)
        {
            // create button
            GameObject newButton = Instantiate(LevelButton) as GameObject;
            LevelButtonNew button = newButton.GetComponent<LevelButtonNew>();

            // button features
            button.LevelText.text = (i+1).ToString();
            button.unlocked = true;
            button.GetComponent<Button>().interactable = true;

            // load level
            button.GetComponent<Button>().onClick.AddListener(() => LevelManager.LoadLevel(Int32.Parse(button.LevelText.text)));

            newButton.transform.SetParent(Spacer);
        }
    }

    private void Save()
    {
        // save level progress
    }
    
}
