/*
GameOverMenu.cs
Script for Pause Menu
Authors: Kevin Likcani, Matthew Mehrmann
Created: 10/8/2023
Revised:
Kevin/Matthew/Eric on 10/8 to fix merge issues.
Jacky Lin: 11/19/2023 - add sound toggle
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PauseMenu : MonoBehaviour
{

    public static bool GameIsPaused = false; //boolean value we will use to check if game is paused
    public GameObject pauseMenuUI; //pauseMenu UI game object
    public GameObject inGameSettingsMenuUI; //in-game settings menu
    public GameObject Player; //player object in game
    public GameObject LevelGrid; //level grid in game

    public Toggle musicToggle;

    void Awake() //Awake function called during instantiation of scene
    {
        /*
            The following things are primers for the scene. We will make sure only the items that we want to see are shown at the start and that game time has been correctly set.
            Additionally, setting timescale to 0 to ensure no game objects move in the background as well as ensuring that game is paused is false. This should be set in the global decleration about;
            however, this is a safety.
        */
        // LevelGrid.SetActive(false);
        // Player.SetActive(false);
        Time.timeScale = 1f;
        GameIsPaused = false;

        //Add listener for when the state of the Toggle changes
        musicToggle.onValueChanged.AddListener(delegate {
            musicToggleChanged(musicToggle);
        });

        // add to player pref
        if (!PlayerPrefs.HasKey("musicOn"))
        {
            PlayerPrefs.SetInt("musicOn", 1);
        }
        Load();
        AudioListener.pause = !musicToggle.isOn;
    }

    void Update()// Update is called once per frame
    {
        if (Input.GetKeyDown(KeyCode.Escape)) //checks if user has pressed the escape button
        {
            if (GameIsPaused) //checks if the GameIsPaused boolean value is set to true
            {
                Resume(); //Calls on Resume method
            }
            else
            {
                Pause(); //Calls on Pause method
            }
        }
    }

    private void CloseMenus()
    {
        pauseMenuUI.SetActive(false);
        inGameSettingsMenuUI.SetActive(false);
    }
    public void GameStart() //function called to start the first time playing
    {
        CloseMenus();
        Player.SetActive(true); //ensure player shows up
        LevelGrid.SetActive(true); //ensures level grid shows up
        Time.timeScale = 1f; //ensures game time gets set correct
        GameIsPaused = false; //set GameIsPaused to false to ensure that escape key work as expected and game recognizes player is playing.
    }

    public void Resume() //Resume method called when resume button is clicked
    {
        CloseMenus();
        inGameSettingsMenuUI.SetActive(false);
        Time.timeScale = 1f;   //controls speed in which games move. when at 1f, game is at moving at normal rate
        GameIsPaused = false; //sets GameIsPaused bool to false
    }

    void Pause() //Pause method called when escape button is hit
    {
        CloseMenus();
        pauseMenuUI.SetActive(true); //activates the Pause Menu UI
        Time.timeScale = 0f;   //controls speed in which games move. when at 0f, game is paused
        GameIsPaused = true;   //sets GameIsPaused bool to false
    }
    public void LoadInGameSettings() //Settings method called when settings button is clicked
    {
        CloseMenus();
        inGameSettingsMenuUI.SetActive(true);
    }

    public void LoadPauseMenu() //loads pause menu from the in game settings screen when player hits the back button
    {
        CloseMenus();
        Pause(); //calls the pause() from above to get pause menu on
    }

    public void LoadMainMenu() //Goes to main menu
    {
        SceneManager.LoadScene("Main Menu");
    }

    private void Save()
    {
        PlayerPrefs.SetInt("musicOn", musicToggle.isOn ? 1 : 0);
    }

    private void Load()
    {
        musicToggle.isOn = PlayerPrefs.GetInt("musicOn") == 1;
    }

    void musicToggleChanged(Toggle change)
    {
        AudioListener.pause = !change.isOn;
        Save();
    }
}
