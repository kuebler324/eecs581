/*
GameOverMenu.cs
Script for game over
Authors: Jacky Lin, Matthew Mehrmann
Created: 10/4/2023
Revised:
Matthew Mehrmann - fix next level button
Jacky Lin: 10/4/2023 - create game over screen
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverMenu : MonoBehaviour
{
    
    public void RestartGame()
    {
        //loads level that player was just on
        LevelManager.ReloadCurrentLevel();
    }

    public void MainMenu()
    {
        //loads main menu
        SceneManager.LoadScene("Main Menu");
    }
    
    //next level function
    public void NextLevel(){
        // Call next level function which is in the LevelManager script.
        LevelManager.LoadNextLevel();
    }

    public void QuitGame()
    {
        //end game
        #if UNITY_EDITOR
            UnityEditor.EditorApplication.isPlaying = false;
        #else
            Application.Quit();
        #endif
    }
}
