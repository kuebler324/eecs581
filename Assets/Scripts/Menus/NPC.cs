/*
NPC.cs
Script for npc tips
Authors: Jacky Lin
Created: 4/7/2024
Revised:
Jacky Lin: 4/7/2024 - npc gives tips when player approaches
*/

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class NPC : MonoBehaviour
{
    public GameObject dialogPanel;
    public TextMeshProUGUI dialogText;
    public string [] dialog;
    private int index;

    // resets the text to beginning after finishing dialog
    public void ResetText()
    {
        index = 0;
        dialogText.text = "";
        dialogPanel.SetActive(false);
    }

    // goes to next dialog statement or ends
    public void Next()
    {
        // checks if there are more dialog
        if (index < dialog.Length - 1)
        {
            dialogText.text = dialog[++index];   
        }
        else
        {
            ResetText();
        }
    }

    // user approaches npc
    private void OnTriggerEnter2D(Collider2D other)
    {
        // show dialog
        if (other.CompareTag("Player"))
        {
            dialogPanel.SetActive(true);
            dialogText.text = dialog[index];
        }
    }

    // user leaves npc
    private void OnTriggerExit2D(Collider2D other)
    {
        // close dialog
        if (other.CompareTag("Player"))
        {
            dialogPanel.SetActive(false);
        }
    }
}
