/*
LevelManager.cs
This is a level manager singleton for switching levels
Authors: Eric Kuebler
Created: 9/20/2023
Revised:
Eric Kuebler: 9/20/2023 - level manager creation
*/
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    public static int levelId = 0; // store current level id

    // allows scenes to load a level based on a provided level ID
    public static void LoadLevel(int levelId)
    {
        LevelManager.levelId = levelId;
        SceneManager.LoadScene("Level " + levelId);
        Debug.Log("Current Level is: " + (LevelManager.levelId));
    }
    // allow reloading the active level
    public static void ReloadCurrentLevel() {
        SceneManager.LoadScene("Level " + LevelManager.levelId);
    }
    // Load the next level 
    public static void LoadNextLevel(){
        SceneManager.LoadScene("Level " + (LevelManager.levelId+1));
        Debug.Log("Next Level is: " + (LevelManager.levelId+1));
    }
}
