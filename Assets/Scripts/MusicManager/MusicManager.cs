using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicManager : MonoBehaviour
{
    // create a protected singleton MusicManager
    public static MusicManager Instance { get; private set; }
    public AudioSource m1; //Defines AudioSource variable to hold m1 segment of audio file
    public AudioSource m2; //Defines AudioSource variable to hold m2 segment of audio file
    public AudioSource m3; //Defines AudioSource variable to hold m3 segment of audio file
    public AudioSource m4; //Defines AudioSource variable to hold m4 segment of audio file
    public AudioSource m5; //Defines AudioSource variable to hold m5 segment of audio file
    public AudioSource m6; //Defines AudioSource variable to hold m6 segment of audio file
    public AudioSource m7; //Defines AudioSource variable to hold m7 segment of audio file
    public AudioSource m8; //Defines AudioSource variable to hold m8 segment of audio file

    public PauseMenu PauseMenu; 

    private int currentTrack; //Defines int variable that keeps track of current audio file segment
    private double dspPlay = 0; //Defines double variable 

    private void Awake()
    {
        if (Instance == null)
        {
            // use this object as the singleton
            Instance = this;
            // prevent game object from being deleted upon changing scenes
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            // remove duplicate if this is not the only instance
            Destroy(gameObject);
        }

    }

    // Start is called before the first frame update
    void Start()
    {
        currentTrack = 7; //sets the current track to m8
        dspPlay = AudioSettings.dspTime; //sets dspPlay variable to current time of audio system 
    }

    void playMusic()
    {
        PauseMenu = FindAnyObjectByType<PauseMenu>();
        if(PauseMenu.GameIsPaused)
        {
            currentTrack = 0; //sets current track to 0
            if(dspPlay - AudioSettings.dspTime <= 0.5) { //checks if Audio file is com
            currentTrack = (currentTrack + 1) % 4; //increments to next track 
            GetAudioSourceByTrack(currentTrack).PlayScheduled(dspPlay);//plays correct audio track
            dspPlay += 9.6; //increments dspPlay variable by 9.6
            }
        }
        else{
            if(dspPlay - AudioSettings.dspTime <= 0.5) { //checks if Audio file is com
            currentTrack = (currentTrack + 1) % 8; //increments to next track 
            GetAudioSourceByTrack(currentTrack).PlayScheduled(dspPlay);//plays correct audio track
            dspPlay += 9.6; //increments dspPlay variable by 9.6 
            }    
        }
    }

    // Update is called once per frame
    void Update()
    {
        playMusic();
    }
    AudioSource GetAudioSourceByTrack(int track) {
        return track switch
        {
            0 => m1, //assigns track number 0 to variable m1 
            1 => m2, //assigns track number 1 to variable m1
            2 => m3, //assigns track number 2 to variable m1
            3 => m4, //assigns track number 3 to variable m1
            4 => m5, //assigns track number 4 to variable m1
            5 => m6, //assigns track number 5 to variable m1
            6 => m7, //assigns track number 6 to variable m1
            7 => m8, //assigns track number 7 to variable m1
            _ => null, //default case
        };
    }
}
