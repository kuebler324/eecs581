using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Stalactite : MonoBehaviour
{
    Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    void OnTriggerEnter2D (Collider2D collide)
    {
        if(collide.gameObject.name.Equals("Player"))
        {
            rb.isKinematic = false;
        }
    }
    private void OnCollisionEnter2D(Collision2D collide) //defines OnCollisionEnter2D method with collision parameter
    {
    if (collide.gameObject.CompareTag("Player")) //checks if object collides with object with the "Player" tag
    {
        // trigger game over screen
        SceneManager.LoadScene("Game Over Menu");
    }
    // destroy coconut self on collision
    Object.Destroy(this.gameObject);
    }
}
