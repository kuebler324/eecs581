const [_path0, _path1, branchName, mergeRequestName] = process.argv;

console.log('received', branchName, mergeRequestName);

if (!/^(ID-[0-9]+-.+)$/.test(branchName)) {
  throw new Error('Branch name does not contain reference to Jira ticket. Example: "ID-123-feature-description"');
}

if (!/^(ID-[0-9]+:.+)$/.test(mergeRequestName)) {
  throw new Error('Merge request name does not contain reference to Jira ticket. Example: "ID-123: Merge request title"');
}
